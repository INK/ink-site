# INK website

## Developing

### Setup

Install dependencies:

```bash
npm install
```

### Editing content

Editable content is in `site/public`. The site is generated from this using [harp.js]().

The easiest way to work on content is to set a development server running:

```
npm run serve
```

Then open http://localhost:10101 in a browser.

Changes you make in `site/public` will be shown in the browser when you save the files and refresh the browser view. No need to re-run the server.
