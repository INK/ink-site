# the free and open web-based job management service

**INK** is a free, open source web-based job management service designed for document and file transformation, through to entity extraction, format validation, enrichment and more.

INK does this by enabling publishing staff to set up and manage these processes through an easy to use web interface and leveraging shared open source converters, validators, extractors etc. In the INK world these individual converters/validators (etc) are called **steps**. Steps can be chained together to form a **recipe**.

Documents can be run through steps and recipes either manually, or by connecting INK to a platform (for example, a Manuscript Submission System). In the later case files can be sent to INK from the platform, processed by INK automatically, and sent back to the original platform without the user doing anything but (perhaps) pushing a button to initiate the process.

INK is integrated into the [INK](http://ink.org) toolkit for building publishing workflows, and is in production use at University of California Press via their INK-based editing platform, [Editoria](http://editoria.pub).
