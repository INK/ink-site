# Server

[![npm](https://img.shields.io/npm/v/ink-server.svg)](https://npmjs.com/package/ink-server)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/ink/ink-server/raw/master/LICENSE)
[![code style standard](https://img.shields.io/badge/code%20style-standard-green.svg)](https://standardjs.com/)
[![coverage report](https://gitlab.coko.foundation/ink/ink-server/badges/master/coverage.svg)](https://gitlab.coko.foundation/ink/ink-server/commits/master)
[![build status](https://gitlab.coko.foundation/ink/ink-server/badges/master/build.svg)](https://gitlab.coko.foundation/ink/ink-server/commits/master)

The `ink-server` module handles the [database layer](#database-layer), contains the core [data models](#data-models), and exposes a [REST API](#rest-api)

<h2 id="database">
  <a name="database"></a>
  Database layer
</h2>

`ink-server` supports [CouchDB](https://couchdb.apache.org/)-compatible storage layers - these are JSON document based noSQL stores that implement the CouchDB API.

In particular we support:

- [PouchDB](https://pouchdb.com) - basic local on-disk storage, useful for development and testing.
- [PouchDB-server](https://github.com/pouchdb/pouchdb-server) - a PouchDB-backed store with an HTTP API and management GUI.
- [CouchDB](https://couchdb.apache.org/) - fully distributable, hostable solution for production environments.

<h2 id="datamodels">
  <a name="datamodels"></a>
  Data models
</h2>

The data models provided in `ink-server` provide a foundation for most publishing applications:

- **knowledge** represented by [`Fragments`](#fragments) which can be organised in [`Collections`](#collections)
- **users** represented by [`Users`](#users) which can be organised in [`Teams`](#teams)
- **rules** governing how models may interact with one another represented by [`Authorize`](#authorize)

<!-- USERS -->

<h3>
  <span id="users">`User`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/models/User.js)</small>
</h3>

#### example usage

```js
const User = require('ink-server/src/models/User')

// create an admin user
const user = new User({
  username: 'admin',
  email: 'admin@website.net',
  password: 'correct-horse-battery-staple',
  admin: true
})

// save to the DB (this populates the object with its ID)
await user.save()

console.log('Saved admin user has ID:', user.id)
```

<!-- COLLECTIONS -->

<h3>
  <span id="collections">`Collection`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/models/Collection.js)</small>
</h3>

A `Collection` is a container for [`Fragment`s](#fragments). It is an abstraction to represent any high-level grouping of published units like a book, a journal, an issue, a blog, and so on. Every INK app must have at least one collection.

#### example usage

```js
const Collection = require('ink-server/src/models/Collection')

const title = 'journalOfKnowledge'
const created = Date.now()

// create a new collection
const collection = new Collection({ title, created })

// set the owner of the collection
// requires that we have a previously saved User instance
collection.setOwners([user.id])

// save to the DB (this populates the object with its ID)
await collection.save()

console.log('Created collection with ID: ', collection.id)
```

<!-- FRAGMENTS -->

<h3>
  <span id="fragments">`Fragment`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/models/Fragment.js)</small>
</h3>

A `Fragment` represents a published unit like a chapter, an article, a blog post, a comment, and so on. `Fragment`s can belong to one or more [`Collection`s](#collections). It can be owned by one or more [`User`s](#users).

### example usage

```js
const Collection = require('ink-server/src/models/Collection')
const journal = await collection.findByField('name', 'journalOfKnowledge')

const Fragment = require('ink-server/src/models/Fragment')

const opts = {
  title: 'A proof that P=NP'
}
const article = new Fragment(opts)

journal.addFragment(article)

article.setOwners([req.user])

console.log(`New team ${article.name} created with ID: ${article.id}`)
```

<!-- TEAMS -->

<h3>
  <span id="teams">`Team`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/models/Team.js)</small>
</h3>

### example usage

```js
// teams are created around collections or fragments
const Collection = require('ink-server/src/models/Collection')
const journal = await findByField('name', 'journalOfKnowledge')

const Team = require('ink-server/src/models/Team')

const opts = {
  name: 'Journal of Knowledge Editors',
  teamType: {
    name: 'Production Editor',
    permissions: 'all'
  },
  object: journal
}
const editors = await new Team(opts)

console.log(`New team ${editors.name} created with ID: ${editors.id}`)
```

<!-- AUTHORIZE -->

<h3>
  <span id="authorize">`Authorize`</span> model <small>[[code on gitlab]](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/models/Authorize.js)</small>
</h3>

Used to determine whether users are authorized to perform requested operations, based on:

- the user
- the type of operation
- the target resource of the operation

### example usage

`Authorize` can be used in isolation, like this:

```js
const Authorize = require('ink-server/src/models/Authorize')

const user = 'user1'
const operation = 'create'
const resource = '/fragments/12345'

const haspermission = Authorize.can(user, operation, resource)

const ornot = haspermission ? '' : ' not'

console.log(`User ${user} does${ornot} have permission to ${operation} ${resource}`)
```

However, it is usually used inside an express route function, checking the authorization data provided in the HTTP request to see if the endpoint operation is permitted.

Here's an example of how a route might use `Authorize`:

```js
const Authorize = require('ink-server/src/models/Authorize')

// add a `delete` endpoint to the API, with authorization
api.delete('/:id', authBearer, (req, res, next) => {
  return Authorize.can(
    req.user, 'delete', req.originalUrl
  ).then(
    () => SomeModel.find(req.params.id)
  ).then(
    team => team.delete()
  ).then(
    team => res.status(STATUS.OK).json(team)
  ).catch(
    next
  )
})
```

You can see real examples of Authorize in use wherever `ink-server` defines an API endpoint, for example [the `GET /teams` endpoint](https://gitlab.coko.foundation/ink/ink-server/blob/master/src/routes/api_teams.js#L38-48).

<h2 id="restapi">
  <a name="restapi"></a>
  REST API
</h2>

`ink-server` ships with a REST API that includes the following endpoints:

<br>

- `/api/collections/:id/teams`
  <span class="api-methods">`GET` `POST`</span>
- `/api/collections/:id/teams/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/api/collections`
  <span class="api-methods">`POST` `GET`</span>
- `/api/collections/:id`
  <span class="api-methods">`GET` `PATCH` `DELETE`</span>
- `/api/collections/:id/fragments`
  <span class="api-methods">`POST` `GET`</span>
- `/api/collections/:collectionId/fragments/:fragmentId`
  <span class="api-methods">`GET` `PATCH` `DELETE`</span>
- `/api/upload`
  <span class="api-methods">`POST`</span>
- `/api/users/authenticate`
  <span class="api-methods">`POST` `GET`</span>
- `/api/users`
  <span class="api-methods">`POST` `GET`</span>
- `/api/users/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/api/teams`
  <span class="api-methods">`GET` `POST`</span>
- `/api/teams/:id`
  <span class="api-methods">`GET` `DELETE` `PUT`</span>
- `/updates`
  <span class="api-methods">`GET`</span>
