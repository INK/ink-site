# Overview

INK is a modern, modular javascript toolkit for building publishing workflows.

The INK core is made up of two modules: [**`ink-server`**](#ink-server) and [**`ink-client`**](#ink-client), both of which can be extended and modified using [**components**](/docs/components/index.html).

<h2 id="server">
  <a name="server"></a>
  **ink-server**
</h2>

A [nodeJS](https://nodejs.org) [express](https://expressjs.com/)-based app which runs on a server. This module contains the data models, handles the database layer, and exposes a REST API and optionally a realtime event stream using Server Sent Events.

<p style="text-align: right;">[...go to `ink-server`](/docs/core/server.html)</p>

<h2 id="client">
  <a name="client"></a>
  **ink-client**
</h2>

A [React](https://facebook.github.io/react/)-based app which runs in the browser. It is written in ES6 and intended to be bundled using Webpack. This module is the base for the user interface for a INK app, interacting with the REST API of the server.

<p style="text-align: right;">[...go to `ink-client`](/docs/core/client.html)</p>

<h2 id="components">
  <a name="components"></a>
  components
</h2>

Both server and client come with some essential functionality, but can be extended with components. A component is a nodeJS module and can export a client component, a server component, or both. Client components can include [React](https://facebook.github.io/react/) components and Redux actions and reducers. Server components can include express middleware or routing functions.

By adding components you can rapidly include and customise features like:

- user management
- realtime sync
- team management
- advanced document editors
- format conversion

... and [much more](/docs/components/library.html).

<p style="text-align: right;">[...go to components](/docs/components/index.html)</p>

## A basic app

A basic INK app is a nodeJS app that has `ink-server` and `ink-client` as dependencies, as well as:

- configuration for the app
- React scaffolding (HTML container, routes, etc.)
- WebPack configuration
- usually, some components

The INK command-line interface [**`ink`**](/docs/core/cli.html) helps with app development. It can generate a basic app, setup the database, run the app, and manage components.

**An example** of the basic structure of an app is [included in the CLI](https://gitlab.coko.foundation/ink/ink-cli/tree/master/initial-app).
