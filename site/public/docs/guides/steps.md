# CLI

[![npm](https://img.shields.io/npm/v/ink-client.svg)](https://npmjs.com/package/ink-client)
[![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/ink/ink-client.svg/raw/master/LICENSE)
[![code style standard](https://img.shields.io/badge/code%20style-standard-green.svg)](https://standardjs.com/)
[![coverage report](https://gitlab.coko.foundation/ink/ink-client/badges/master/coverage.svg)](https://gitlab.coko.foundation/ink/ink-client)
[![build status](https://gitlab.coko.foundation/ink/ink-client/badges/master/build.svg)](https://gitlab.coko.foundation/ink/ink-client)

The `ink` module provides a command-line interface for generating, managing and running INK apps.

## Getting INK CLI

### Prerequisites

- node v7.7+
- yarn v0.24+ (required to run `ink new`)
- npm v5+

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage nodeJS and nvm versions. See [detailed instructions on installing prerequisites](/docs/misc/prerequisites.html) on Mac, Linux, and PC.

### Installation

The INK command-line tools can be installed from `npm` or `yarn`:

```bash
npm install --global ink
```

or

```bash
yarn global add ink
```

## Using INK CLI

### Displaying the commands (`ink` or `ink help`)

Outputs:

```bash
Usage: ink [options] [command]


Commands:

  new         create and set up a new ink app
  run         start a ink app
  setupdb     generate a database for a ink app
  add         add one or more components to a ink app
  adduser     add a user to the database for a ink app
  help [cmd]  display help for [cmd]

Options:

  -h, --help     output usage information
  -V, --version  output the version number
```

For any command, you can see detailed usage help by running `ink help cmd`, e.g.:

```bash
ink help new
ink help run
# ...etc
```

### Generating an app (`ink new`)

The `new` subcommand generates a template INK app for you to work from. This includes everything needed to run your publishing platform: dependencies, database setup, boilerplate code and configuration, and a set of initial components.

To generate an app, just provide the name:

```bash
ink new myappname
```

`ink` will create a subdirectory with the name you supplied, and start generating the app inside. It'll ask you a series of questions to customise your app. If you prefer to provide the answers in the initial command, you can do that instead:

```bash
ink new myappname \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --collection Articles
```

### Running your app (`ink run`)

The `run` subcommand starts your app. It takes care of transpilation, module bundling and process management.

To start your app:

**either** use the `run` command from the app directory:

```bash
cd myappname
ink run
```

**or** provide the app path to the `run` command:

```bash
ink run path/to/myapp
```

### Setting up the database (`ink setupdb`)

The `setupdb` subcommand creates the database for your app. It is usually used when you've cloned the source code of an existing app, or when you want to start over with a fresh database.

To generate a database for an app that doesn't have one yet:

```bash
ink setupdb ./path/to/app/dir
```

By default this will generate a **production** database only. To generate a **dev** database, run the command with `--dev`:

```bash
ink setupdb --dev ./path/to/app/dir
```

If your app already has a database, `ink setupdb` will not overwrite it by default. You can force it to delete an existing database and overwrite it with a new one using `--clobber`:

```bash
$ ink setupdb ./path/to/app/dir
info: Generating INK app database at path api/db/production
error: Database appears to already exist
error: If you want to overwrite the database, use --clobber
$ ink setupdb --clobber ./path/to/app/dir
info: Generating INK app database at path api/db/production
info: Database appears to already exist
info: Overwriting existing database due to --clobber flag
info: setting up the database
info: building prompt
question:><Admin username><
[...etc]
```

As with `ink new`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```bash
ink setupdb ./path/to/app/dir \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --collection Articles
```

### Adding a user to the database (`ink adduser`)

You can add a user to an existing database:

```bash
ink adduser ./path/to/app/dir
```

You can optionally make that user an admin:

```bash
ink adduser --admin ./path/to/app/dir
```

As with `ink new` and `ink setupdb`, you can skip any or all of the interactive prompts by providing the user details with command-line flags:

```text
ink adduser ./path/to/app/dir \
  --username someuser \
  --email some@email.com \
  --password correct-horse-battery-staple \
  --admin true
```
