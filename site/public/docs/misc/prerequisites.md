# Installing Prerequisites

* [Mac (OSX)](#mac)
* [Linux (Ubuntu v16.04)](#linux)

<h2><span id="mac">Mac (OSX):</span></h2>

### 1. Install and configure `git`

Use this installer to install git: https://git-scm.com/download/mac

Once `git` is installed, configure it with the following commands, with your own name and email:
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### 2. Install `nvm`

We recommend using [`nvm`](https://github.com/creationix/nvm) to install and manage nodeJS and nvm versions. To get nvm, run:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

### 3. Install `nodeJS`

To install or switch to node v7.7 with nvm, run:
```bash
nvm install 7.7
```

### 4. Use `npm` v5.3

To install or switch to npm v5.3, run:
```bash
npm install -g npm@5.3
```

### 5. Install `yarn`

Run the following command:
```bash
curl -o- -L https://yarnpkg.com/install.sh | bash
```

### 6. Double check versions

To double check that you are using the correct node and npm versions, run  
`nvm current`: should be 7.7.4  
`npm --version`: should be 5.3.0  
If these are correct, you should be ready to [install INK](/docs/core/cli.html)

<h2><span id="linux">Linux (Ubuntu v16.04):</span></h2>

### 1. Prepare for tool installation
```bash
sudo apt-get update
sudo apt-get install build-essential libssl-dev
```

### 2. Install and configure `git`

```bash
sudo apt-get install git-all
```
Once `git` is installed, configure it with the following commands, with your own name and email:
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### 3. Install `nvm`
Download the nvm installation script:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```
Restart the terminal to complete installation.

### 4. Install `nodeJS`

To install or switch to node v7.7 with nvm, run:
```bash
nvm install 7.7
```

### 5. Use `npm` v5.3

To install or switch to npm v5.3, run:
```bash
npm install -g npm@5.3
```

### 6. Install `yarn`

Run the following command:
```bash
curl -o- -L https://yarnpkg.com/install.sh | bash
```
Restart the terminal to complete installation.

### 7. Double check versions

To double check that you are using the correct node and npm versions, run  
`nvm current`: should be 7.7.4  
`npm --version`: should be 5.3.0  
If these are correct, you should be ready to [install INK](/docs/core/cli.html)
