# Projects using INK

## Editoria

[Editoria](https://gitlab.coko.foundation/editoria/editoria) is a book editing platform built in collaboration with the University of California Press by Coko Foundation, and in particular by the Editoria team based in Athens. Editoria will soon be deployed for book production at UCP.

- [Source code repository](https://gitlab.coko.foundation/editoria/editoria)
- [Issue tracker](https://gitlab.coko.foundation/editoria/editoria/issues)
- [Discussion room](https://mattermost.coko.foundation/coko/channels/editoria)

## xpub

[*xpub*](https://gitlab.coko.foundation/xpub/xpub) is a journal platform under development by Coko Foundation. It showcases how INK can be used for journal production by combining pre-existing components with a new custom components.

- [Source code repository](https://gitlab.coko.foundation/xpub/xpub)
- [Issue tracker](https://gitlab.coko.foundation/xpub/xpub/issues)
