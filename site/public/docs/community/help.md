# Getting help

**If you have a general query about INK**, or want to discuss anything with us, come and [chat to us in our Mattermost channel](https://mattermost.coko.foundation/coko/channels/ink).

**Bug reports and feature requests** belong in the issues of the relevant repository:
  - [`ink-server`](https://gitlab.coko.foundation/ink/ink-server/issues)
  - [`ink-client`](https://gitlab.coko.foundation/ink/ink-client/issues)
  - [`ink-cli`](https://gitlab.coko.foundation/ink/ink-cli/issues)

**If you're not sure where an issue belongs**, or to raise an issue about the INK ecosystem in general, [use the ink/ink issues](https://gitlab.coko.foundation/ink/ink/issues).
